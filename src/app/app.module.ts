import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { QuestionsComponent } from './questions/questions.component';
import { EntryFieldComponent } from './entry-field/entry-field.component';
import { QuestionService } from '../shared/question.service';
import { FormsModule } from '@angular/forms';
import { ShadowDirective } from './directives/shadow-answer';
import { openAndClosePrompt } from './directives/openAndClosePrompt';


@NgModule({
  declarations: [
    AppComponent,
    QuestionsComponent,
    EntryFieldComponent,
    ShadowDirective,
    openAndClosePrompt
  ],
  imports: [
    BrowserModule,
    FormsModule
  ],
  providers: [QuestionService],
  bootstrap: [AppComponent]
})
export class AppModule { }
