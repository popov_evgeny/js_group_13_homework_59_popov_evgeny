import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { QuestionModel } from '../../shared/question.model';
import { QuestionService } from '../../shared/question.service';

@Component({
  selector: 'app-questions',
  templateUrl: './questions.component.html',
  styleUrls: ['./questions.component.css']
})
export class QuestionsComponent implements OnInit {

  @Input() index = 0;

  @Output() point = new EventEmitter<boolean>();

  questions!: QuestionModel[];

  inputFieldIsEmpty = false;

  openOrClose = false;

  constructor(private questionService: QuestionService) {}

  ngOnInit(): void {
    this.questions = this.questionService.addQuestions();
  }

  getQuestion(userAnswer: string) {
    this.point.emit();
    this.inputFieldIsEmpty = userAnswer === '';
    this.questionService.question = this.questions[this.index].question;
    this.questionService.onCheckTheAnswer(userAnswer);
  }

  openOrClosePrompt() {
    this.openOrClose = this.questionService.openOrClose;
    this.questionService.openOrClose = !this.questionService.openOrClose;
  }
}
