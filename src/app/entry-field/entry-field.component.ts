import { Component, ElementRef, EventEmitter, OnInit, Output, ViewChild } from '@angular/core';
import { QuestionModel } from '../../shared/question.model';
import { QuestionService } from '../../shared/question.service';

@Component({
  selector: 'app-entry-field',
  templateUrl: './entry-field.component.html',
  styleUrls: ['./entry-field.component.css']
})
export class EntryFieldComponent implements OnInit {
  @ViewChild('answerInput') answerInput!: ElementRef;

  @Output() onClickCheckAnswer = new EventEmitter<string>();

  questions!: QuestionModel[];

  constructor(private questionService: QuestionService) {}

  ngOnInit(): void {
    this.questions = this.questionService.addQuestions();
  }

  onClickBtn() {
    this.onClickCheckAnswer.emit(this.answerInput.nativeElement.value);
  }
}
