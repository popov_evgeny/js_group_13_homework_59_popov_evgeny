import { Component, OnInit } from '@angular/core';
import { QuestionService } from '../shared/question.service';
import { QuestionModel } from '../shared/question.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{

  questions!: QuestionModel[];

  countQuestions = 0;
  countCorrectAnswer = 0;

  constructor(private questionService: QuestionService) {

  }

  ngOnInit(): void {
    this.questions = this.questionService.addQuestions();
    this.countQuestions = this.questions.length;
    this.questionService.changeQuestions.subscribe((questions:QuestionModel[]) => {
      this.questions = questions;
    });
  }
}

