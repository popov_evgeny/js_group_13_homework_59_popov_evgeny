import { Directive, ElementRef, HostListener, Input, Renderer2 } from '@angular/core';


@Directive({
  selector: '[appShadow]'
})

export class ShadowDirective {

  @Input() set appShadow(shadowClass: string) {
    if (shadowClass) {
      this.shadowClass = shadowClass;
    }
  }

  constructor(private el: ElementRef,  private renderer: Renderer2) {
  }

  shadowClass = 'no answer';

  @HostListener('submit')addClass() {
    if (this.shadowClass === 'correct answer') {
      this.renderer.addClass(this.el.nativeElement, 'shadowGreen');
    } else if (this.shadowClass === 'wrong answer') {
      this.renderer.addClass(this.el.nativeElement, 'shadowRed');
    } else {
      this.renderer.addClass(this.el.nativeElement, 'shadow');
    }
  }
}
