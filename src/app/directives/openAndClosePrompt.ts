import { Directive, ElementRef, HostListener, Renderer2 } from '@angular/core';
import { QuestionService } from '../../shared/question.service';


@Directive({
  selector: '[appPrompt]'
})

export class openAndClosePrompt {

  constructor(private el: ElementRef,  private renderer: Renderer2, private questionService: QuestionService) {
  }

  @HostListener('mousedown')addClass() {

    if (this.questionService.openOrClose) {
      this.renderer.removeClass(this.el.nativeElement, 'd-block');
      this.renderer.addClass(this.el.nativeElement, 'd-none');
    } else {
      this.renderer.removeClass(this.el.nativeElement, 'd-none');
      this.renderer.addClass(this.el.nativeElement, 'd-block');
    }
  }

}

