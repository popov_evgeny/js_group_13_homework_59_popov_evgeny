import { QuestionModel } from './question.model';
import { EventEmitter } from '@angular/core';

export class QuestionService {
  changeQuestions = new EventEmitter<QuestionModel[]>();
  question = '';
  responseStatus = 'no answer';
  openOrClose = true;

  private questions: QuestionModel[] = [
    new QuestionModel('2 x 2 ?', '4', '2 + 2 ?', ''),
    new QuestionModel('3 x 2 ?', '6', '3 + 3 ?', ''),
    new QuestionModel('4 x 2 ?', '8', '4 + 4 ?', ''),
    new QuestionModel('5 x 2 ?', '10', '5 + 5 ?', ''),
    new QuestionModel('6 x 2 ?', '12', '6 + 6 ?', ''),
  ];

  addQuestions() {
    for (let i = 0; i < this.questions.length; i++) {
      this.questions[i].responseStatus = this.responseStatus;
    }
    return this.questions.slice();
  }

  onCheckTheAnswer(userAnswer: string) {
    this.changeQuestions.emit(this.questions);
    const question = this.questions.find(question => question.question === this.question);
    if (question) {
      if (userAnswer === '') {
        question.responseStatus = 'no answer';
      } else {
        if (question.answer === userAnswer){
          question.responseStatus = 'correct answer';
        } else {
          question.responseStatus = 'wrong answer'
        }
      }
    }
  }
}
